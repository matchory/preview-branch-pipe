FROM bitnami/minideb:buster

RUN set -eux; \
    apt-get -qq update; \
    apt-get -qq install --yes --no-install-recommends bash jq git wget unzip curl ca-certificates > /dev/null; \
    rm -rf /var/lib/apt/lists/*; \
    wget --quiet --output-document "aws-cli.zip" \
        "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"; \
    unzip -qq "aws-cli.zip"; \
    ./aws/install --install-dir /usr/local/aws-cli --bin-dir /usr/local/bin; \
    wget --quiet --output-document "/usr/local/bin/lightsailctl" \
        "https://s3.us-west-2.amazonaws.com/lightsailctl/latest/linux-amd64/lightsailctl"; \
    chmod +x /usr/local/bin/lightsailctl; \
    rm -rf ./aws "aws-cli.zip"

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

RUN set -eux; \
    wget --quiet --directory-prefix "/" \
        https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh; \
    chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
