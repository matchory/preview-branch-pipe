# Bitbucket Pipelines Pipe: Branch Preview Pipe
This pipe creates a preview deployment of the current branch on AWS Lightsail.

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: matchory/branch-preview-pipe:0.4.1
    variables:
      CONFIGURATION_URL: "<string>"
      CONFIGURATION_PASSWORD: "<string>"
      CONFIGURATION_USER: "<string>" # Optional
      SERVICE_NAME_SUFFIX: "<string>" # Optional
      TEAMS_PAYLOAD_FILE: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```

## Variables
| Variable                   | Usage                                                                                 |
| -------------------------- | ------------------------------------------------------------------------------------- |
| CONFIGURATION_URL (*)      | The URL to pull the deployment configuration from                                     |
| CONFIGURATION_PASSWORD (*) | The token to authenticate to the configuration URL with                               |
| CONFIGURATION_USER         | The useranme to authenticate with. Default: `branch-preview`.                         |
| SERVICE_NAME_SUFFIX        | Suffix to append to the service name. Default: `preview`.                             |
| TEAMS_PAYLOAD_FILE         | File to write the Microsoft Teams webhook payload file to. Default: `./payload.json`. |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                    |

_(*) = required variable._

## Prerequisites
You need to have an AWS account and identity configured as repository variables, with permissions to access Lightsail.

## Examples

Simple example:
```yaml
script:
  - pipe: matchory/branch-preview-pipe:0.4.1
    variables:
      CONFIGURATION_URL: $PREVIEW_CONFIG_URL
      CONFIGURATION_PASSWORD: $PREVIEW_CONFIG_TOKEN
```

Advanced example:
```yaml
script:
  - pipe: matchory/branch-preview-pipe:0.4.1
    variables:
      CONFIGURATION_URL: $PREVIEW_CONFIG_URL
      CONFIGURATION_USER: $PREVIEW_CONFIG_USER
      CONFIGURATION_PASSWORD: $PREVIEW_CONFIG_TOKEN
      SERVICE_NAME_SUFFIX: demo
      TEAMS_PAYLOAD_FILE: dist/teams-payload.json
      DEBUG: 'true' # watch for the quotes
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by moritz@matchory.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
