# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.1

- patch: Moved to matchory Docker org

## 0.4.0

- minor: Implemented TEAMS_PAYLOAD_FILE variable
- patch: Fixed notification title

## 0.3.0

- minor: Implemented proper service naming

## 0.2.19

- patch: Added Teams payload to local directory too

## 0.2.18

- patch: Fixed permissions

## 0.2.17

- patch: Removed JQ for debugging

## 0.2.16

- patch: Added fallback value

## 0.2.15

- patch: Fixed state parsing

## 0.2.14

- patch: Switched to JQ

## 0.2.13

- patch: Fixed bad state field

## 0.2.12

- patch: Fixed missing variable

## 0.2.11

- patch: Fixed deployment config manifest

## 0.2.10

- patch: Fixed configuration server failing 2

## 0.2.9

- patch: Fixed configuration server failing

## 0.2.8

- patch: Added CA certificates

## 0.2.7

- patch: Added curl as a dependency

## 0.2.6

- patch: Added hadolint
- patch: Fixed build issues
- patch: Improved Dockerfile
- patch: Switched to minideb base image

## 0.2.5

- patch: Removed iconv step

## 0.2.4

- patch: Added patched iconv dependency

## 0.2.3

- patch: Added missing git dependency

## 0.2.2

- patch: Fixed target image name

## 0.2.1

- patch: Improved log output

## 0.2.0

- minor: Add SERVICE_NAME_SUFFIX option

## 0.1.0

- minor: Initial release

