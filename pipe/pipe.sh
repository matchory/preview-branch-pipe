#!/usr/bin/env bash
#
# This pipe creates a preview deployment of the current branch on AWS Lightsail.
#

source "$(dirname "$0")/common.sh"

# Required parameters
CONFIGURATION_URL=${CONFIGURATION_URL:?'CONFIGURATION_URL variable missing.'}
CONFIGURATION_USERNAME=${CONFIGURATION_USERNAME:?'CONFIGURATION_USERNAME variable missing.'}
CONFIGURATION_PASSWORD=${CONFIGURATION_PASSWORD:?'CONFIGURATION_PASSWORD variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}
SERVICE_NAME_SUFFIX=${SERVICE_NAME_SUFFIX:="preview"}
TEAMS_PAYLOAD_FILE=${TEAMS_PAYLOAD_FILE:="./payload.json"}

# Build the short commit hash
short_commit_hash=$(git log --pretty=format:'%h' -n 1)

# Build a slug from the branch name
# shellcheck disable=SC2019 disable=SC2018
branch_slug=$(
  echo "$BITBUCKET_BRANCH" |
  sed -E 's/[^a-zA-Z0-9-]+/-/g' |
  sed -E 's/^-+|-+$//g' | tr A-Z a-z
)

service_label="${branch_slug}-${BITBUCKET_BUILD_NUMBER}"
service_name_prefix="${BITBUCKET_REPO_SLUG}-${branch_slug}"
prefix_length=$(( 63 - ${#SERVICE_NAME_SUFFIX} ))
service_name="${service_name_prefix:0:$prefix_length}-${SERVICE_NAME_SUFFIX}"
image="${service_label}:${short_commit_hash}"

info "Deploying service as ${image}"

# Build the preview image
docker build -t "$image" .

# Fetch container service, create if the service doesn't exist yet
if aws lightsail get-container-services --service-name "$service_name" 2> /dev/null; then
  debug "A container service for ${service_name} exists already"
else
  run aws lightsail create-container-service \
    --service-name "$service_name" \
    --power micro \
    --scale 1

  debug "Created container service for ${service_name}"
fi

# Upload the application image
run aws lightsail push-container-image \
  --region="$AWS_DEFAULT_REGION" \
  --service-name="$service_name" \
  --label="$service_label" \
  --image="$image"

debug "Pushed container image ${image}:${service_label} for service ${service_name}"

# Fetch the image name from Lightsail
image_identifier=$(
  aws lightsail get-container-images \
    --service-name "$service_name" \
    --query 'sort_by(containerImages, &createdAt)[-1].image' \
    --output text
)

debug "Container image was identified as ${image_identifier}"

# Fetch the deployment URL from Lightsail
deployment_url=$(
  aws lightsail get-container-services \
    --service-name "$service_name" \
    --query 'containerServices[0].url' \
    --output text
)

debug "Container service ${service_name} is hosted on ${deployment_url}"

deployment_config_manifest=$(cat <<EOF
{
    "repository": "$BITBUCKET_REPO_FULL_NAME",
    "branch": "$BITBUCKET_BRANCH",
    "commit": "$BITBUCKET_COMMIT",
    "imageName": "$image_identifier",
    "serviceName": "$service_name",
    "containerName": "$service_label",
    "deploymentUrl": "$deployment_url"
}
EOF
)

# shellcheck disable=SC2154
debug "Requesting deployment configuration using manifest: ${deployment_config_manifest}"

# Fetch the deployment configuration
# TODO: Ideally, we would fetch an OAuth access token here, which would allow
#       provisioning the preview. At that point, we'll probably want dedicated
#       infrastructure around preview branches! Have fun :)
deployment_config=$(
  curl "$CONFIGURATION_URL" \
    --header "User-Agent: preview-branch-pipe/1.0 (Matchory; BitBucket) curl" \
    --user "${CONFIGURATION_USERNAME}:${CONFIGURATION_PASSWORD}" \
    --header "Content-Type: application/json; charset=utf-8" \
    --header "Accept: application/json" \
    --data "${deployment_config_manifest}" \
    --fail \
    --silent || fail "Failed to fetch configuration"
)
deployment_config=${deployment_config:="{}"}

debug "Received deployment configuration: $(echo $deployment_config)"

# Create a new deployment
run aws lightsail create-container-service-deployment \
  --service-name "$service_name" \
  --cli-input-json "$deployment_config"

debug "Created container deployment"

counter=0
deployment_status="NONE"

# Wait for the deployment to finish for 72*5s = 360s = 6 minutes
while [ "$counter" -le 72 ] && [ "$deployment_status" != "RUNNING" ]; do

  # Kind of a back-off timer; the deployment usually takes 1-4 minutes
  sleep 5

  ((counter = counter + 1))

  debug "Checking deployment status... ($counter)/72"

  # https://lightsail.aws.amazon.com/ls/docs/en_us/articles/amazon-lightsail-container-services-deployments#creating-deployment-status
  deployment_status=$(aws lightsail get-container-services --service-name "$service_name" --query 'containerServices[0].state' --output text)

  debug "Current deployment status: $deployment_status"
done

# If the counter reached 72, the deployment status didn't change
if [ "$counter" -ge 72 ]; then
  fail "Deployment did not finish in time. Check the Lightsail console manually."
fi

debug "Deployment finished successfully"

# Build a change log including all changes from (including) the one that
# triggered the pipeline until the repository head, and transform that into a
# FactSet JSON structure (Array<{ title: string; value: string }>).
git_change_log=$(
  git log --oneline "$BITBUCKET_COMMIT"~1...HEAD |
  jq -Rnc '[inputs | match("(.+?) (.+)") | { title: (.captures[0].string), value: .captures[1].string }]'
)

# Build the Teams webhook payload
# shellcheck disable=SC2016
printf '{"type":"message","attachments":[{"contentType":"application/vnd.microsoft.card.adaptive","contentUrl":null,"content":{"type":"AdaptiveCard","$schema":"http://adaptivecards.io/schemas/adaptive-card.json","version":"1.2","body":[{"type":"TextBlock","text":"Branch Preview Ready","wrap":true,"weight":"Bolder"},{"type":"TextBlock","text":"The %s branch preview is ready now.","wrap":true,"spacing":"None"},{"type":"Container","items":[{"type":"TextBlock","text":"Recent Changes","wrap":true,"weight":"Bolder"},{"type":"FactSet","facts":%s}]},{"type":"ColumnSet","columns":[{"type":"Column","width":"stretch","items":[{"type":"ActionSet","actions":[{"type":"Action.OpenUrl","title":"Preview","url":"%s","style":"positive"},{"type":"Action.OpenUrl","title":"Repository","url":"https://bitbucket.org/%s"}]}]},{"type":"Column","width":"auto","items":[{"type":"TextBlock","text":"#%s","wrap":true,"horizontalAlignment":"Right","maxLines":1,"isSubtle":true,"fontType":"Monospace"}],"verticalContentAlignment":"Center"}],"spacing":"ExtraLarge","separator":true}]}}]}' \
  "$BITBUCKET_BRANCH" \
  "$git_change_log" \
  "$deployment_url" \
  "$BITBUCKET_REPO_FULL_NAME" \
  "$short_commit_hash" > "$BITBUCKET_PIPE_STORAGE_DIR/teams-webhook-payload.json"
chmod 777 "$BITBUCKET_PIPE_STORAGE_DIR/teams-webhook-payload.json"
cp "$BITBUCKET_PIPE_STORAGE_DIR/teams-webhook-payload.json" "$TEAMS_PAYLOAD_FILE"

debug "Wrote notification payload to $TEAMS_PAYLOAD_FILE"

info "Deployed service to $deployment_url"
